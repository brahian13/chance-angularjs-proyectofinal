appModule.service('phalconService', function($http) {

	var phalconService = {};

	phalconService.add = function(ruta, datos, successFunction){
		var url = 'http://localhost/phalcon/api/' + ruta;
		$http({
			method: 'POST',
			url: url,
			data: angular.toJson(datos, true)
		}).then(function successcallback(response) {
			console.log(response.data);
			if (angular.fromJson(response.data).status ==="OK") {
				if (successFunction != undefined) {
					successFunction();
				};
			}
		});
	 };

	
	 phalconService.buscar = function( ruta , id , successFunction ){
		
		$http({
			method: 'GET',
			url: 'http://localhost/phalcon/api/'+ ruta + "/" + id
		}).then(function successcallback(response) {
			
			if (successFunction != undefined) {
					successFunction(response.data);
				};
		}, function errorCallback(response) {
			console.log(response.data);
		});
	 };


	 phalconService.buscarEmp = function( ruta , fecha ,id, successFunction ){
		
		$http({
			method: 'GET',
			url: 'http://localhost/phalcon/api/'+ ruta + "/" +fecha+ "/" + id
		}).then(function successcallback(response) {
			
			if (successFunction != undefined) {
					successFunction(response.data);
				};
		}, function errorCallback(response) {
			console.log(response.data);
		});
	 };


	 phalconService.getLista = function(ruta,successFunction){
		
		$http({
			method: 'GET',
			url: 'http://localhost/phalcon/api/'+ruta
		}).then(function successcallback(response) {
			
			if (successFunction != undefined) {
					successFunction(response.data);
				};
		}, function errorCallback(response) {
			console.log(response.data);
		});
	 };

	  phalconService.elimi = function(ruta,id, successFunction)
	 {
	 	$http({
		method: 'DELETE',//metodo utilizado
		url: 'http://localhost/phalcon/api/'+ruta+"/"+id,

		
		}).then(function successcallback(response) {
		//funcion en caso de tener exito
		console.log(response.data);
			if (successFunction != undefined) {
					successFunction();
				};
		}, function errorCallback(response) {
			if (angular.fromJson(response.data).status==="ERROR") {
				alert(angular.fromJson(response.data).messages[0]);
			}
		});
	 };
	 
	 phalconService.actualizar=function(ruta,user,successFunction){

	 		$http({
			method: 'PUT',
			url: 'http://localhost/phalcon/api/'+ruta+'/'+user.id,
			data: angular.toJson(user, true)
		}).then(function successcallback(response) {
				if (successFunction != undefined) {
					successFunction();
				};
		}, function errorCallback(response) {
			if (angular.fromJson(response.data).status==="ERROR") {
				alert(angular.fromJson(response.data).messages[0]);
			}
		});
	 };

	return phalconService;
});