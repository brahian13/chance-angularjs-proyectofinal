var appModule= angular.module('app', [
	'ngRoute',
	'ui.router',
	'ngAside',
	'ui.bootstrap.tpls',
	'mobile-angular-ui']);



appModule.run(function($rootScope, $location, sessionData){
	$rootScope.rutas={
		menuLateralEmpleado: "templates/empleado/aside.html",
		menuLateralAdmin: "templates/admin/aside.html"
	};

	var user = window.localStorage.getItem("user");
	if(user){
		user = JSON.parse(user);
		$rootScope.user = user;
		sessionData.user = user;

		if(user.tipo == 'Empleado'){
			$location.path('/menuEmpleado/home');
		}
		else
			if(user.tipo == 'Administrador'){
			$location.path('/menuAdmin/home');
		}
	}

	//Usar para eliminar la variable de localstorage cuando cierre sesion
	//window.localStorage.removeItem("user");
});

appModule.config(['$stateProvider','$urlRouterProvider',
	function($stateProvider, $urlRouterProvider){
		
		$stateProvider
	    .state('login', {
	      	url: "/login",
	      	templateUrl: "templates/general/login.html",
	      	controller: 'loginCtrl'
	    })
	    .state('menuEmpleado', {
	      	url: "/menuEmpleado",
	      	abstract: true,
	      	templateUrl: "templates/empleado/menuEmpleado.html"
	    })
	    .state('menuEmpleado.home', {
	      	url: "/home",
	      	templateUrl: "templates/general/home.html"
	    })
	    .state('menuEmpleado.registrarChance', {
	      	url: "/registrarChance",
	      	templateUrl: "templates/empleado/registrarChance.html",
	      	controller : 'Chancectrl'
	    })
 		.state('menuEmpleado.mensajeEmp', {
	      	url: "/mensajeEmp",
	      	templateUrl: "templates/empleado/mensajeEmp.html",
	      	controller: 'msjCtrlEmp'
	    })
	    .state('menuEmpleado.balance', {
	      	url: "/balance",
	      	templateUrl: "templates/empleado/balance.html",
	      	controller:'balanCtrlEmp'
	    })
	    .state('menuEmpleado.resultados', {
	      	url: "/resultados",
	      	templateUrl: "templates/general/resultados.html",
	      	controller : 'resultadosCtrl'
	    })
	    .state('menuEmpleado.contactenos', {
	      	url: "/contactenos",
	      	templateUrl: "templates/general/contactenos.html"
	    })
	    .state('menuAdmin', {
	      	url: "/menuAdmin",
	      	abstract: true,
	      	templateUrl: "templates/admin/menuAdmin.html"
	    })
	     .state('menuAdmin.home', {
	      	url: "/home",
	      	templateUrl: "templates/general/home.html"
	    })
	    .state('menuAdmin.mapa', {
	      	url: "/mapa",
	      	templateUrl: "templates/admin/mapa.html",
	      	controller: 'mapaCtrl'
	    })
	    .state('menuAdmin.registrarEmpleado', {
	      	url: "/registrarEmpleado",
	      	templateUrl: "templates/admin/registrarEmpleado.html",
	      	controller : 'appCTRL'
	    })
	    .state('menuAdmin.editarChance', {
	      	url: "/editarChance",
	      	templateUrl: "templates/admin/editarChance.html",
	      	controller : 'appCTRLlote'
	    })
	    .state('menuAdmin.dashboard', {
	      	url: "/dashboard",
	      	templateUrl: "templates/admin/dashboard.html"
	    })
	    .state('menuAdmin.balance', {
	      	url: "/balance",
	      	templateUrl: "templates/admin/balance.html",
	      	controller:'balanCtrlAdmin'
	    })
	    .state('menuAdmin.mensajeAdmin', {
	      	url: "/mensajeAdmin",
	      	templateUrl: "templates/admin/mensajeAdmin.html",
	      	controller:'mensCtrl'
	    })	    
	    .state('menuAdmin.resultados', {
	      	url: "/resultados",
	      	templateUrl: "templates/general/resultados.html",
	      	controller : 'resultadosCtrl'
	    })
	     .state('menuAdmin.contactenos', {
	      	url: "/contactenos",
	      	templateUrl: "templates/general/contactenos.html"
	    })
	     .state('menuAdmin.datosEmpleado', {
	      	url: "/datosEmpleado/:nombre/:id/:age/:direccion/:telefono/:username/:tipo",
	      	templateUrl: "templates/admin/datosEmpleado.html", 
	      	controller: 'datosEmpleadoCtrl' ,function($stateParams){
			      $stateParams.nombre //*** Watch Out! DOESN'T EXIST!! ***//
			      $stateParams.id //*** Exists! ***// 
			      $stateParams.age //*** Watch Out! DOESN'T EXIST!! ***//
			      $stateParams.direccion 
			      $stateParams.telefono //*** Watch Out! DOESN'T EXIST!! ***//
			      $stateParams.username 
			      $stateParams.tipo  
			   }
	    })
	     .state('menuEmpleado.msj', {
	      	url: "/msj/:msj",
	      	templateUrl: "templates/empleado/msj.html", 
	      	controller: 'unMsjCtrl' ,function($stateParams){
			      $stateParams.msj
			   }
	    })
	     .state('404', {
	      	url: "/404",
	      	templateUrl: "templates/general/404.html",
	      	controller:  function($scope, $state){
	      		$scope.go = function(){
	      			var user = window.localStorage.getItem("user");
					if(user){
						user = JSON.parse(user);
						if(user.tipo == 'Empleado'){
							$state.go('menuEmpleado.home');
						}
						else if(user.tipo == 'Administrador'){
							$state.go('menuAdmin.home');
						}
					}
					else{
						$state.go('login');
					}
	      		}
	      	}
	    });


	    $urlRouterProvider.otherwise("/404");
	}])

appModule.value('sessionData', {});

	appModule.directive('editableTduser', [function() {
	   return {
	      restrict: 'A',
	      link: function(scope, element, attrs) {
	        element.css("cursor", "pointer");
	        element.attr('contenteditable', 'true');
	        element.bind('blur keyup change', function() {
	            scope.users[attrs.row][attrs.field] = element.text();
	        });
	        element.bind('click', function() {
	            document.execCommand('selectAll', false, null);
	        });
	      }
	   };
	}]);


appModule.directive('editableTd', [function() {
   return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        element.css("cursor", "pointer");
        element.attr('contenteditable', 'true');
        element.bind('blur keyup change', function() {
            scope.lista[attrs.row][attrs.field] = element.text();
        });
        element.bind('click', function() {
            document.execCommand('selectAll', false, null);
        });
      }
   };
}]);