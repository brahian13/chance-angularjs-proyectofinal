appModule.controller('MainAdminCtrl', function($scope, $aside ,$rootScope,sessionData, phalconService) {
	
	$scope.asideState = {
		open: false
	};

	$rootScope.user = sessionData.user;
	
	$scope.openAside = function(position, backdrop) {
		$scope.asideState = {
			open: true,
			position: position
		};
		
		function postClose() {
			$scope.asideState.open = false;
		}
		
		$aside.open({
			templateUrl: 'templates/admin/aside.html',
			placement: position,
			backdrop: backdrop,
			controller: function($scope, $uibModalInstance) {
				$scope.ok = function(e) {
					$uibModalInstance.close();
					e.stopPropagation();
				};
				$scope.cancel = function(e) {
					$uibModalInstance.dismiss();
					e.stopPropagation();
				};
			}
		}).result.then(postClose, postClose);
	}

	$rootScope.user.actual='line';
	phalconService.actualizar('users', $rootScope.user);

	$scope.cerrarSession= function()
	{

		window.localStorage.removeItem("user");
		
	}
});

appModule.controller('mapaCtrl', function(phalconService){
	console.log('aaaa');

	function loadJS() {
	    var jsElm = document.createElement("script");
	    jsElm.type = "application/javascript";
	    jsElm.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBn6cbDQ4CBOJyCb0fp5rl7AAahDFNZreA&callback=initMap';
	    document.body.appendChild(jsElm);
	}
	function cargarPosiciones(){
		var successFunction = function(data){
			posicionesMapa = data;
			loadJS();
		}
		phalconService.getLista('posiciones', successFunction);
	}
	cargarPosiciones();
});

var map;
var posicionesMapa;
var initMap = function() {
	  map = new google.maps.Map(document.getElementById('map'), {
	    center: {lat: 4.539315593825067, lng: -75.68506137874759},
	    zoom: 12
	  });

  	for (var i = 0; i < posicionesMapa.length; i++) {

  		var posicion = {lat: parseFloat(posicionesMapa[i].latitud) , lng: parseFloat(posicionesMapa[i].longitud)}
  		var marker = new google.maps.Marker({
		    map: map,
		    position: posicion,
		    title: posicionesMapa[i].id
		  });
  	};
}
//creamos un controlador llamado appCTRL
appModule.controller('appCTRL', function($scope, $http, phalconService) {

	//codigo del controlador
	$scope.user = {};

	$scope.userss = {};

	//aqui se almacenaran todos los usuarios que hay en la BD
	$scope.users= [];

	$scope.deleteUser = function(id){
	//objeto usado para hacer peticionas ajax
		phalconService.elimi('users', id, $scope.getUsers);
	 };
	 
	$scope.updateUser = function(user){
		$http({
			method: 'PUT',
			url: 'http://localhost/phalcon/api/users/'+user.id,
			data: angular.toJson(user, true)
		}).then(function successcallback(response) {
			console.log(angular.fromJson(response.data).status);
			if (angular.fromJson(response.data).status==="OK") {
				$scope.getUsers();
			};
		}, function errorCallback(response) {
			if (angular.fromJson(response.data).status==="ERROR") {
				alert(angular.fromJson(response.data).messages[0]);
			}
		});
	 };

	


	 $scope.addUser = function(id){
	 	
	 	var successFunction = function(data){

			if(data.status=="OK"){
				alert('Ya existe un usuario con el mismo ID');
			}else
			{
				edad = document.getElementById("edad").value;
					if (edad>18 && edad<60) 
					{
						tipo = document.getElementById("tipo").selectedIndex;
						if (tipo!=null) {
							if($scope.user.id<1095000000 && $scope.user.id>1000000)
							{
								$scope.user.estado='Desbloqueado';
								$scope.user.actual='noline';
								phalconService.add('users', $scope.user, $scope.getUsers);
								$scope.user = {};
							}
							else{
								alert('la id no es correcto');
							}
						}
						else{
							alert('Selecciona un tipo: Empleado o Administrador');
						}
					}else{
						alert('Debe ser mayor de edad y tener menos de 60 años');
					}
			}
	 	};
		phalconService.buscar('users',id, successFunction);

	};

	
	 $scope.updateBloqueado = function(user){
	 		
	 		if (user.estado!="Bloqueado") {
	 			user.estado='Bloqueado';
				phalconService.actualizar('users', user, $scope.getUsers);
	 		}else{
	 			user.estado='Desbloqueado';
				phalconService.actualizar('users', user, $scope.getUsers);
	 		}
		 	
		 };
	

	$scope.getUsers = function(){
		
		var successFunction = function(data){
			$scope.users = data;
		};

		phalconService.getLista('users',successFunction);
	 };
	 //ejectuamos el metodo getUsers para poder ejecutar todos
	 $scope.getUsers();



});

appModule.controller('mensCtrl', function($scope, $http, phalconService){

	$scope.user = {};

	$scope.userss = {};

	$scope.mensaje={};

	$scope.getUser = function(){ 
		var successFunction = function(data){
				$scope.userss = data;
			};

			phalconService.getLista('users',successFunction);
	};

	$scope.getUser();


	$scope.enviar = function(){

		$scope.fecha = new Date();
		$scope.dd = $scope.fecha.getDate();
		$scope.mm = $scope.fecha.getMonth()+1; 
		$scope.yyyy = $scope.fecha.getFullYear();

		$scope.hora = $scope.fecha.getHours();
		$scope.minutos = $scope.fecha.getMinutes(); 

		if($scope.dd<10) {
		    $scope.dd='0'+$scope.dd
		} 

		if($scope.mm<10) {
		    $scope.mm='0'+$scope.mm
		} 

		$scope.fecha = $scope.mm+'-'+$scope.dd+'-'+$scope.yyyy+' Hora: '+$scope.hora+':'+$scope.minutos;

		$scope.mensaje.fecha = $scope.fecha;
		$scope.mensaje.estado='false';

		phalconService.add('mensajes', $scope.mensaje);

		$scope.mensaje = {};

	};

});

appModule.controller('balanCtrlAdmin', function($scope, $http, phalconService){

	$scope.user = {};

	$scope.userss = {};

	$scope.mensaje={};

	$scope.getUser = function(){ 
		var successFunction = function(data){
				$scope.userss = data;
			};

			phalconService.getLista('users',successFunction);
	};

	$scope.getUser();


	$scope.chance = {};
	$scope.chances = {};

	$scope.getchance = function(){ 
		var successFunction = function(data){
				$scope.chances = data;
				getTotal(data);
			};

			phalconService.getLista('chances',successFunction);
	};

	$scope.getchance();

	$scope.balanceTotal=function(){
		$scope.getchance();
	};

	$scope.filtrar = function(fecha)
	{
		$scope.hoy = fecha;
		$scope.dd = $scope.hoy.getDate();
		$scope.mm = $scope.hoy.getMonth()+1; //hoy es 0!
		$scope.yyyy = $scope.hoy.getFullYear();

		if($scope.dd<10) {
		    $scope.dd='0'+$scope.dd
		} 

		if($scope.mm<10) {
		    $scope.mm='0'+$scope.mm
		} 

		$scope.hoy = $scope.mm+'-'+$scope.dd+'-'+$scope.yyyy;

		console.log($scope.hoy);

		var successFunction = function(data){
			$scope.chances = data;
			getTotal(data);
		};
		
		phalconService.buscar('chances/search', $scope.hoy ,successFunction);

	}


	$scope.filtrar1 = function(fecha,id)
	{
		
		$scope.hoy = fecha;
		$scope.dd = $scope.hoy.getDate();
		$scope.mm = $scope.hoy.getMonth()+1; //hoy es 0!
		$scope.yyyy = $scope.hoy.getFullYear();

		if($scope.dd<10) {
		    $scope.dd='0'+$scope.dd
		} 

		if($scope.mm<10) {
		    $scope.mm='0'+$scope.mm
		} 

		$scope.hoy = $scope.mm+'-'+$scope.dd+'-'+$scope.yyyy;


		var successFunction = function(data){
			$scope.chances = data;
			getTotal(data);
		};

		phalconService.buscarEmp('chances/search', $scope.hoy , id, successFunction);

	}

	var getTotal = function(data){
		$scope.total = 0;
		for (var i = 0; i < data.length; i++) {
			$scope.total +=parseFloat(data[i].valor);
		};
		console.log($scope.total);
	};

});


appModule.controller('appCTRLlote', function($scope, $http, phalconService) {


	$scope.loteria = {};
	$scope.loteriaEli = {};
	$scope.loterias={};
	
	$scope.addLoteria = function(){
		
		phalconService.add('loterias', $scope.loteria, $scope.getLoterias);
		$scope.loteria = {};

	};

	$scope.getLoterias = function(){
		
		var successFunction = function(data){
			$scope.loterias = data;
		};

		phalconService.getLista('loterias',successFunction);
	 };
	 //ejectuamos el metodo getUsers para poder ejecutar todos
	 $scope.getLoterias();

	$scope.eliminarLoteria = function(){
		console.log($scope.loteriaEli.nombre);
		phalconService.elimi('loterias', $scope.loteriaEli.nombre, $scope.getLoterias);
	};

	$scope.numeroEli = {};
	$scope.numero = {};
	$scope.numeros={};
	
	$scope.addNumero = function(){
		
		
		if($scope.numero.numero<10000){
			$scope.numero.tipo="bloqueado";
			phalconService.add('numeros', $scope.numero, $scope.getNumeros);
			$scope.numero= {};
		}
		else
		{
			alert("El numero debe ser de 3 cifras");
		}
	};

	$scope.getNumeros = function(){
		
		var successFunction = function(data){
			$scope.numeros = data;
		};

		phalconService.getLista('numeros',successFunction);

	 };
	 //ejectuamos el metodo getUsers para poder ejecutar todos
	 $scope.getNumeros();

	 

	 $scope.desbloquearNumero = function(){
	 	console.log($scope.numeroEli.numero);
		phalconService.elimi('numeros', $scope.numeroEli.numero, $scope.getNumeros);
	};
});



appModule.controller('resultadosCtrl', function($scope) {
	var thumbnailSliderOptions =
	{
	    sliderId: "thumbnail-slider",
	    orientation: "horizontal",
	    thumbWidth: "50%",
	    thumbHeight: "auto",
	    showMode: 3,
	    autoAdvance: true,
	    selectable: true,
	    slideInterval: 3000,
	    transitionSpeed: 1000,
	    shuffle: false,
	    startSlideIndex: 0, //0-based
	    pauseOnHover: true,
	    initSliderByCallingInitFunc: false,
	    rightGap: 0,
	    keyboardNav: true,
	    mousewheelNav: false,
	    before: null,
	    license: "mylicense"
	};

	var mcThumbnailSlider = new ThumbnailSlider(thumbnailSliderOptions);
	console.log('hola');
});




appModule.controller('datosEmpleadoCtrl', function($scope, $stateParams){
		
		$scope.nombre = $stateParams.nombre;
		$scope.id=$stateParams.id ; 
		$scope.age= $stateParams.age ;
		$scope.direccion=$stateParams.direccion ;
		$scope.telefono= $stateParams.telefono ;
		$scope.username=$stateParams.username ;
		$scope.tipo=$stateParams.tipo;  
	});