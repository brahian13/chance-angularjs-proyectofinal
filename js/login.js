appModule.controller('loginCtrl', function($scope, $http, $location, $state, sessionData, phalconService){
	

	$scope.user = {}

	$scope.login = function(){

		
			$http({
				method: 'POST',
				url: 'http://localhost/phalcon/api/users/login', 
				data: angular.toJson($scope.user, true)
			}).then(function successcallback(response) {
				if (response.data.data!=undefined) {
				if(response.data && response.data.data){

					var user = response.data.data;
					sessionData.user = user;
					window.localStorage.setItem("user", JSON.stringify(user));
					if(user.tipo == 'Administrador'){
						$state.go('menuAdmin.home');
						console.log("1");
					}
					else{
							if(user.tipo == 'Empleado')
							{	
								if (user.estado=='Desbloqueado') {
									$state.go('menuEmpleado.home');
								}
								else
								{
									alert('El usuario se encuentra bloqueado, Contate al administrador');
								}
								
							}
							else
							{
							}

						}
					
					console.log(sessionData.user);
				}
			}
				else
				{
					alert('El username y la contraseña son incorrectos');
				}
				

			}, function errorCallback(response) {
				console.log('error', response);
			});

		
	}

});