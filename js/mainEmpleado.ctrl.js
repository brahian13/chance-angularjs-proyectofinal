appModule.controller('MainEmpleadoCtrl', function($scope, $aside, $rootScope, $window, sessionData, phalconService) {
	

	var guardarPosicion = function(){
		if (navigator.geolocation) {
		    navigator.geolocation.getCurrentPosition(function(position) {
		    	var pos = {
		    		latitud: position.coords.latitude,
		        	longitud: position.coords.longitude,
		        	id: sessionData.user.id
		      	};
		      	phalconService.add('posicion', pos);

		    }, function() {
		      handleLocationError(true, infoWindow, map.getCenter());
		    });
		} else {
		    // Browser doesn't support Geolocation
		    handleLocationError(false, infoWindow, map.getCenter());
		}
	}
	guardarPosicion();

	$scope.asideState = {
		open: false
	};

	$rootScope.user = sessionData.user;

	
	$scope.openAside = function(position, backdrop) {
		$scope.asideState = {
			open: true,
			position: position
		};
		
		function postClose() {
			$scope.asideState.open = false;
		}
		
		$aside.open({
			templateUrl: 'templates/empleado/aside.html',
			placement: position,
			backdrop: backdrop,
			controller: function($scope, $uibModalInstance) {
				$scope.ok = function(e) {
					$uibModalInstance.close();
					e.stopPropagation();
				};
				$scope.cancel = function(e) {
					$uibModalInstance.dismiss();
					e.stopPropagation();
				};
			}
		}).result.then(postClose, postClose);
	}

	
	$rootScope.user.actual='line';
	phalconService.actualizar('users', $rootScope.user);

	$scope.cerrarSession= function()
	{
		window.localStorage.removeItem("user");
		phalconService.elimi('posiciones',sessionData.user.id);
	}

	$window.onunload = function(){
		phalconService.elimi('posiciones',sessionData.user.id);
	}
});

appModule.controller('Chancectrl', function($scope, $http, $rootScope,sessionData,phalconService) {

	$scope.chance = {
		'signo': 'Sin signo'
	};
	$scope.total = 0;
	$scope.totalGan = 0;
	$scope.lista = {};

	$scope.eliminar = function(key, valor) {
	   if (confirm("¿Seguro que desea eliminar?")) {
	      $scope.total -= valor;
	      $scope.totalGan -= (valor*1000);
	      delete $scope.lista[key];
	   }
	};

	$scope.addChance = function(num) {

		var successFunction = function(data){
			
			if(data.status=="FOUND"){
				alert("El numero se encuentra Bloqueado");
			}else
				{
					if($scope.chance.numero<10000){
					if($scope.chance.serie<1000){
						if ($scope.chance.valor<10001 && $scope.chance.valor>100) {

								$scope.total += $scope.chance.valor;
								$scope.totalGan += ($scope.chance.valor*1000)
								$scope.lista[Object.keys($scope.lista).length] = $scope.chance;
								$scope.chance = {
									'signo': 'Sin signo'
								};
							}	
							else
							{
								alert("El limite Maximo es de $10000 por cada Apuesta y el minimo es de $100");
							}			
						}	
						else
						{
							alert("La serie debe ser de 2 cifras");
						}		
					}
					else
					{
						alert("El numero debe ser de 4 cifras");
					}
				}
			
		};

		phalconService.buscar('numeros',num,successFunction);

		

	};

	$scope.vender = function(){

		$scope.hoy = new Date();
		$scope.dd = $scope.hoy.getDate();
		$scope.mm = $scope.hoy.getMonth()+1; //hoy es 0!
		$scope.yyyy = $scope.hoy.getFullYear();

		$scope.hora = $scope.hoy.getHours();
		$scope.minutos = $scope.hoy.getMinutes(); 

		if($scope.dd<10) {
		    $scope.dd='0'+$scope.dd
		} 

		if($scope.mm<10) {
		    $scope.mm='0'+$scope.mm
		} 

		$scope.hoy = $scope.mm+'-'+$scope.dd+'-'+$scope.yyyy+' Hora: '+$scope.hora+':'+$scope.minutos;


		for (var key in $scope.lista) {
			$scope.lista[key].vendedor = sessionData.user.id;
			$scope.lista[key].fecha=$scope.hoy;
			phalconService.add('chances', $scope.lista[key]);
		};
		$scope.lista = {};
		$scope.total="";
	};

	$scope.loteria = {};
	$scope.loterias={};

	$scope.getLoterias = function(){
		
		var successFunction = function(data){
			$scope.loterias = data;
		};

		phalconService.getLista('loterias',successFunction);
	 };
	 //ejectuamos el metodo getUsers para poder ejecutar todos
	 $scope.getLoterias();
});


appModule.controller('balanCtrlEmp', function($scope, $http, phalconService,sessionData){

	$scope.chance = {};
	$scope.chances = {};

	


	$scope.filtrar = function(fecha,id)
	{
		id=	sessionData.user.id
		$scope.hoy = fecha;
		$scope.dd = $scope.hoy.getDate();
		$scope.mm = $scope.hoy.getMonth()+1; //hoy es 0!
		$scope.yyyy = $scope.hoy.getFullYear();

		if($scope.dd<10) {
		    $scope.dd='0'+$scope.dd
		} 

		if($scope.mm<10) {
		    $scope.mm='0'+$scope.mm
		} 

		$scope.hoy = $scope.mm+'-'+$scope.dd+'-'+$scope.yyyy;


		var successFunction = function(data){
			$scope.chances = data;
			getTotal(data);
		};

		phalconService.buscarEmp('chances/search', $scope.hoy , id, successFunction);

	}
	var getTotal = function(data){
		$scope.total = 0;
		for (var i = 0; i < data.length; i++) {
			$scope.total +=parseFloat(data[i].valor);
		};
	};

});


appModule.controller('msjCtrlEmp', function($scope, $http , sessionData , phalconService ) {

	
	$scope.mensajes={};

	var successFunction = function(data){
				$scope.mensajes = data;
			};

	phalconService.buscar('mensajes', sessionData.user.id ,successFunction);

	$scope.mensaje = {};
	
		$scope.abrirMensaje = function(mensaje) {
		
			mensaje.estado='true';
			$http({
			method: 'PUT',
			url: 'http://localhost/phalcon/api/mensajes/'+mensaje.id,
			data: angular.toJson(mensaje, true)
			}).then(function successcallback(response) {
				console.log(angular.fromJson(response.data).status);
				if (angular.fromJson(response.data).status==="OK") {
					
				};
			}, function errorCallback(response) {
				if (angular.fromJson(response.data).status==="ERROR") {
					alert(angular.fromJson(response.data).messages[0]);
				}
			});
		};

});



appModule.controller('resultadosCtrl', function($scope) {
	var thumbnailSliderOptions =
	{
	    sliderId: "thumbnail-slider",
	    orientation: "horizontal",
	    thumbWidth: "50%",
	    thumbHeight: "auto",
	    showMode: 3,
	    autoAdvance: true,
	    selectable: true,
	    slideInterval: 3000,
	    transitionSpeed: 1000,
	    shuffle: false,
	    startSlideIndex: 0, //0-based
	    pauseOnHover: true,
	    initSliderByCallingInitFunc: false,
	    rightGap: 0,
	    keyboardNav: true,
	    mousewheelNav: false,
	    before: null,
	    license: "mylicense"
	};

	var mcThumbnailSlider = new ThumbnailSlider(thumbnailSliderOptions);
	console.log('hola');
});

appModule.controller('unMsjCtrl', function($scope, $stateParams){
		
		

		$scope.msj = $stateParams.msj;

});